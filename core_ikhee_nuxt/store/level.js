import axios from 'axios'

// state
export const state = () => ({
  list: []
})

// getters
export const getters = {
  list: state => state.list,
}

// mutations
export const mutations = {
  SET_LIST (state, list) {
    state.list = list
  },

  PUSH (state, payload) {
    state.list.unshift(payload)
  },

  REPLACE (state, payload) {
    state.list[state.list.map(function(e) { return e.id }).indexOf(payload.id)] = payload
  },

  REMOVE (state, payload) {
    state.list.splice(state.list.map(function(e) { return e.id }).indexOf(payload.id), 1)
  },
}

// actions
export const actions = {
  async fetch ({ commit }) {
    try {
      const { data } = await axios.get('/level')
      commit('SET_LIST', data)
    } catch (e) {
      console.error(e)
    }
  },

  push ({ commit }, payload) {
    commit('PUSH', payload)
  },

  remove ({ commit }, payload) {
    commit('REMOVE', payload)
  },

  update ({ commit }, payload) {
    commit('REPLACE', payload)
  }
}
