export default ({ store, redirect }) => {
  let user = store.getters['auth/user']
  if (store.getters['auth/check']) {
    if(user.role === 'admin'){
      return
    }
  }
  return redirect('/')
}
