export default ({ store, redirect }) => {
  let user = store.getters['auth/user']
  if (store.getters['auth/check']) {
    if(user.role === store.getters['roles/role'].TEACHER){
      return
    }
  }
  return redirect('/')
}
