import colors from 'vuetify/es5/util/colors'
require('dotenv').config()
const { join } = require('path')
const { copySync, removeSync } = require('fs-extra')

export default {
  ssr: false,
  /*
  ** Headers of the page
  */
  env: {
    apiUrl: process.env.API_URL + '/api' || process.env.APP_URL + '/api',
    hostUrl: process.env.API_URL || process.env.APP_URL,
    appName: process.env.APP_NAME || 'Laravel Nuxt',
    appLocale: process.env.APP_LOCALE || 'mn',
    githubAuth: !!process.env.GITHUB_CLIENT_ID
  },

  head: {
    titleTemplate: '%s - ' + process.env.npm_package_name,
    title: process.env.APP_NAME,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || 'Монголын хамгийн том онлайн сургалт болно. Код бичих, маркетинг, мэдээлэл технологи' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/logo/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '@/assets/css/global.css'
  ],

  router: {
    middleware: ['locale', 'check-auth']
  },

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    './plugins/mixins/user.js',
    './plugins/mixins/validations.js',
    './plugins/axios.js',
    './plugins/vform.js',
    '~plugins/i18n',
    '~plugins/nuxt-client-init',
    '@/plugins/nuxt-plyr',
    '@/plugins/vuetify.js',
    '@/plugins/tiptap.js',
    '@/plugins/vue-img.js',
    { src: '~plugins/ga.js', mode: 'client' }
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/vuetify',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios',
  ],
  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    optionsPath: '~plugins/vuetify.options.js',
    theme: {
      dark: true,
      themes: {
        dark: {
          primary: colors.red.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        },
        light: {
          primary: '#532593',
          secondary: '#b0bec5',
          anchor: '#8c9eff',
        },
      }
    }
  },
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: '/login',
            method: 'post',
            propertyName: 'meta.token'
          },
          user: {
            url: 'user',
            method: 'get',
            propertyName: 'data'
          },
          logout:{
            url: 'logout',
            method: 'post'
          }
        }
      }
    },
    redirect: {
      login: '/auth/login',
      home: '/'
    },
    plugins: [
      './plugins/authUrl'
    ]
  },
  axios: {
    baseURL: process.env.APP_URL + '/api'
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extractCSS: true,
    extend (config, ctx) {
    }
  }
}
