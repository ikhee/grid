<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 10, 'role' => 'admin', 'first_name' => 'admin', 'email'=>'test@test.com', 'login_name' => 'admin', 'password'=>bcrypt('admin')],
            ['id' => 7, 'role' => 'teacher', 'first_name' => 'Test1', 'email'=>'test1@test.com', 'login_name' => 'teacher', 'password'=>bcrypt('teacher')],
        ];
        foreach ($items as $item) {
            User::updateOrCreate(['id' => $item['id']], $item);
        }
        // $mail = Str::random(10);
        // DB::table('users')->insert([
        //     'role' => 'admin',
        //     'first_name' => Str::random(10),
        //     'email' => $mail . '@gmail.com',
        //     'login_name' => $mail . '@gmail.com',
        //     'password' => Hash::make('password'),
        // ]);
    }
}
