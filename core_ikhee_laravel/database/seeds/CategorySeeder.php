<?php
use App\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
              "id" => 1,
              "name" => "Бизнес",
              "img" => "/temp/business.png",
              "description" => "Стартап, бикнес төсөл",
              "icon" => "/icons/business.svg"
            ],
            [
              "id" => 2,
              "name" => "Мэдээлэл технологи",
              "img" => "/temp/technology.jpg",
              "description" => "Вэб сайт хийх, Апп бүтээх",
              "icon" => "/icons/information-technology.svg"
            ],
            [
              "id" => 3,
              "name" => "Хувь хүний хөгжил",
              "img" => "/temp/self-development.jpg",
              "description" => "Хандлага, харилцаа",
              "icon" => "/icons/self-development.svg"
            ],
            [
              "id" => 4,
              "name" => "Оффис чадвар",
              "img" => "/temp/business.png",
              "description" => "",
              "icon" => "/icons/office-program.svg"
            ],
            [
              "id" => 5,
              "name" => "Лайфстайл",
              "img" => "/temp/life-style.png",
              "description" => "",
              "icon" => "/icons/lifestyle.svg"
            ],
            [
              "id" => 6,
              "name" => "Санхүү хөрөнгө оруулалт",
              "img" => "/temp/finance.png",
              "description" => "",
              "icon" => "/icons/finance.svg"
            ],
            [
              "id" => 7,
              "name" => "Маркетинг",
              "img" => "/temp/marketing.png",
              "description" => "",
              "icon" => "/icons/marketing.svg"
            ]
          ];


        foreach ($items as $item) {
            Category::updateOrCreate(['id' => $item['id']], $item);
        }
    }
}
