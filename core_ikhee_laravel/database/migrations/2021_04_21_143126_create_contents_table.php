<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->mediumText('video_url');
            $table->mediumText('description')->nullable();
            $table->bigInteger('group_id')->unsigned()->index();
            $table->integer('expired_time')->nullable();
            $table->boolean('is_free')->nullable(); // Худалдан авалт хийгээгүй хэрэглэгчид харагдах эсэх
            $table->smallInteger('type_id'); // lecture or exam id
            $table->integer('order_no')->nullable();
            $table->timestamps();
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
}
