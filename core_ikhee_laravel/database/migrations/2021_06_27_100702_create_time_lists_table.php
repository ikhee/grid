<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimeListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('time_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->index();
            $table->smallInteger('t00')->nullable()->default(0);
            $table->smallInteger('t01')->nullable()->default(0);
            $table->smallInteger('t02')->nullable()->default(0);
            $table->smallInteger('t03')->nullable()->default(0);
            $table->smallInteger('t04')->nullable()->default(0);
            $table->smallInteger('t05')->nullable()->default(0);
            $table->smallInteger('t06')->nullable()->default(0);
            $table->smallInteger('t07')->nullable()->default(0);
            $table->smallInteger('t08')->nullable()->default(0);
            $table->smallInteger('t09')->nullable()->default(0);
            $table->smallInteger('t10')->nullable()->default(0);
            $table->smallInteger('t11')->nullable()->default(0);
            $table->smallInteger('t12')->nullable()->default(0);
            $table->smallInteger('t13')->nullable()->default(0);
            $table->smallInteger('t14')->nullable()->default(0);
            $table->smallInteger('t15')->nullable()->default(0);
            $table->smallInteger('t16')->nullable()->default(0);
            $table->smallInteger('t17')->nullable()->default(0);
            $table->smallInteger('t18')->nullable()->default(0);
            $table->smallInteger('t19')->nullable()->default(0);
            $table->smallInteger('t20')->nullable()->default(0);
            $table->smallInteger('t21')->nullable()->default(0);
            $table->smallInteger('t22')->nullable()->default(0);
            $table->smallInteger('t23')->nullable()->default(0);
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_lists');
    }
}
