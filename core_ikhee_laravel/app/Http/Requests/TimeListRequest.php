<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TimeListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            't00' => 'nullable',
            't01' => 'nullable',
            't02' => 'nullable',
            't03' => 'nullable',
            't04' => 'nullable',
            't05' => 'nullable',
            't06' => 'nullable',
            't07' => 'nullable',
            't08' => 'nullable',
            't09' => 'nullable',
            't10' => 'nullable',
            't11' => 'nullable',
            't12' => 'nullable',
            't13' => 'nullable',
            't14' => 'nullable',
            't15' => 'nullable',
            't16' => 'nullable',
            't17' => 'nullable',
            't18' => 'nullable',
            't19' => 'nullable',
            't20' => 'nullable',
            't21' => 'nullable',
            't22' => 'nullable',
            't23' => 'nullable',
            'user_id' => 'required | unique'
        ];
    }
}
