<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        // return Auth::user()->role === 'admin';
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'first_name' => 'required',
            'login_name' => 'required|unique:users,login_name,'.$this->id,
            'role' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => 'required',
            'first_name.required' => 'required',
            'email.email' => 'Мэйл хаягаа зөв оруулна уу.',
            'login_name' => 'Заавал оруулах талбараа оруулна уу.',
            'role.required' => 'Хэрэглэгчийн үүрэг заавал оруулна уу.',
        ];
    }
}
