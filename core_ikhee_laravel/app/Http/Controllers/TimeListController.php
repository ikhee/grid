<?php

namespace App\Http\Controllers;

use App\Http\Requests\TimeListRequest;
use App\Models\TimeList;
use Illuminate\Http\Request;
use App\Http\Resources\TimeList as TimeListResource;

class TimeListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return TimeListResource::collection(TimeList::paginate(15));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TimeListRequest $request)
    {
        $validated = $request->validated();
        return new TimeListResource(TimeList::create($validated));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TimeList  $timeList
     * @return \Illuminate\Http\Response
     */
    public function show(TimeList $timeList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TimeList  $timeList
     * @return \Illuminate\Http\Response
     */
    public function edit(TimeList $timeList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TimeList  $timeList
     * @return \Illuminate\Http\Response
     */
    public function update(TimeListRequest $request, TimeList $timeList)
    {
        $validated = $request->validated();
        $timeList->update($validated);
        return new TimeListResource($timeList);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TimeList  $timeList
     * @return \Illuminate\Http\Response
     */
    public function destroy(TimeList $timeList)
    {
        $timeList->delete();
    }
}
