<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUser;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\UserDetailRsource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminUserController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        // return UserResource::collection(User::get());
        return UserResource::collection(User::paginate(15));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUser $request){
        $validated = $request->validated();
        // $validated['login_name'] = Auth::user()->school_id;
        $validated['role'] = 'student';
        $validated['password'] = bcrypt('secret');
        $user = User::create($validated);
        return new UserResource($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUser $request, $id){
        $validated = $request->validated();
        // $this->validate($request,[
        //     'login_name' => 'unique|users,login_name,'.$id
        // ]);
        $user = User::find($id);
        $user->update($validated);
        return new UserResource($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $user = User::find($id);
        $user->delete();
    }

    public function getUser($id){
        return new UserDetailRsource(User::find($id));
    }
}
