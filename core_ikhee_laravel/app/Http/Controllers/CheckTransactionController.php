<?php

namespace App\Http\Controllers;

use App\CheckTransaction;
use App\Http\Requests\CheckTransactionRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckTransactionController extends Controller
{
    //



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CheckTransactionRequest $request)
    {
        $request['user_id'] = Auth::user()->id;
        $request['expiry_day'] = 90;
        return CheckTransaction::store($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(CheckTransaction $group)
    {
        $group->delete();
    }
}
