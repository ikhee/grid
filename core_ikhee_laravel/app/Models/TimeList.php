<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TimeList extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        't00',
        't01',
        't02',
        't03',
        't04',
        't05',
        't06',
        't07',
        't08',
        't09',
        't10',
        't11',
        't12',
        't13',
        't14',
        't15',
        't16',
        't17',
        't18',
        't19',
        't20',
        't21',
        't22',
        't23',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
