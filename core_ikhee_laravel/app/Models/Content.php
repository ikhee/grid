<?php

namespace App\Models;

use App\Group;
use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'course_id',
        'video_url',
        'description',
        'expired_time',
        'is_free',
        'type_id'
    ];

    public static function store($request){
        $course = new self;
        $course->title = $request->title;
        $course->group_id = $request->group_id;
        $course->video_url = $request->video_url;
        $course->description = $request->description;
        $course->expired_time = $request->expired_time;
        $course->is_free = $request->is_free;
        $course->type_id = $request->type_id;
        $course->order_no = $request->order_no;
        $course->save();
        return $course;
    }

    public function course(){
        return $this->belongsTo(Course::class);
    }

    public function group(){
        return $this->belongsTo(Group::class);
    }
}
