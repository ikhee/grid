<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckTransaction extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'course_id',
        'expiry_day'
    ];

    public static function store($request){
        $course = new self;
        $course->user_id = $request->user_id;
        $course->course_id = $request->course_id;
        $course->expiry_day = $request->expiry_day;
        $course->save();
        return response()->json(['transaction' => true]);
    }
}
