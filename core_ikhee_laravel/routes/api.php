<?php

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/user', 'MeController@current');
    Route::post('/logout', 'AuthController@logout');
    Route::post('/movephoto', 'PhotoUploadController@movePhoto');
    Route::apiResource('/users', 'AdminUserController');
    Route::get('/getUser/{id}', 'AdminUserController@getUser');
    Route::get('/getowncourse', 'CourseController@getowncourse');
    Route::apiResource('/course/transaction', 'CheckTransactionController');
});

Route::post('/register', 'AuthController@register');

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('/login', 'AuthController@login');
});

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::get('/me', 'MeController@index');
});

Route::apiResource('/course', 'CourseController');
Route::apiResource('/content', 'ContentController');
Route::apiResource('/category', 'CategoryController');
Route::apiResource('/level', 'LevelController');
Route::get('/content1', 'ContentController@read');
Route::get('/getNewCourse', 'CourseController@getNewCourse');


Route::apiResource('/group', 'GroupController');
Route::post('/uploadFile', 'CourseController@uploadFile');
Route::apiResource('/time-lists', 'TimeListController');

Route::get('/test', function () {
    // return response()->json([
    //     'id' => 1,
    //     'event' => 'Medehgui ee'
    // ], 200)->header('Content-Type', 'text/event-stream');

    // return response()->stream(function () {
    //     while (true) {
    //         if (connection_aborted()) {
    //             break;
    //         }
    //         if ($messages = Carbon::now()) {
    //             echo "id: 1\n", "event: ping\n", "data: {$messages}", "\n\n";
    //         }
    //         ob_flush();
    //         flush();
    //         sleep(5);
    //     }
    // }, 200, [
    //     'Cache-Control' => 'no-cache',
    //     'Content-Type' => 'text/event-stream',
    // ]);
});
