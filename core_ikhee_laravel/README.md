# SCHOOL

``` bash
## Server асаах үед
$ php artisan serve
# Column өөрчилснийг мэдрэхэд тулсана
$ composer require doctrine/dbal
# Cache болон Log цэвэрлэнэ
$ php artisan cache:clear
$ php artisan config:clear
# session болон token үүсгэхэд ашиглагдах түүлийг агуулдаг учир заавал уншуулах ёстой
$ php artisan key:generate
$ php artisan jwt:secret
$ php artisan cache:clear
$ php artisan config:clear

#Хэрэгтэй терминалын код
$ php artisan make:request NameRequest

# Public file үүсгэж өгнө
$ php artisan storage:link

```
